import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { CartComponent } from './components/cart/cart.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
const routes: Routes = [
  {
    path: 'dashboard/:customerId',
    component: DashboardComponent
  },
  {
    path: 'home',
    component: LoginComponent,

  },
  {
    path: 'book-detail/:customerId/:bookId',
    component: BookDetailComponent,
  },
  {
    path: 'cart/:customerId',
    component: CartComponent
  },
  {
    path: 'order-details/:customerId',
    component: OrderDetailsComponent
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
