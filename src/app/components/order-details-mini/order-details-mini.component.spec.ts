import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsMiniComponent } from './order-details-mini.component';

describe('OrderDetailsMiniComponent', () => {
  let component: OrderDetailsMiniComponent;
  let fixture: ComponentFixture<OrderDetailsMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailsMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
