import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book } from '../../models/book';

@Component({
  selector: 'app-order-details-mini',
  templateUrl: './order-details-mini.component.html',
  styleUrls: ['./order-details-mini.component.css']
})
export class OrderDetailsMiniComponent implements OnInit {

  @Input() key;
  @Input() value;

  book: Book;
  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.bookService.getBookById(this.key).subscribe(
      data => { this.book = data; },
      err => { console.log(err); },
      () => {}
    );
  }

}
