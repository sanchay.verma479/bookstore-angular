export class Book {
    id: number ;
    name: String ;
    author: String;
    publication: String;
    availableQuantity: number;
    price: number;
}
