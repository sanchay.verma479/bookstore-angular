import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../models/book';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  private rootUrl: String = 'http://localhost:8080/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*'
    })
   };
  constructor(private http: HttpClient) { }

  getAllBooks(): Observable<any> {
    return this.http.get(this.rootUrl + '/book');
  }
  getBookById(bookId): Observable<any> {
    return this.http.get(this.rootUrl + '/book/' + bookId );
  }
}
