import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private rootUrl: String = 'http://localhost:8080/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
    })
   };
  constructor(private http: HttpClient) { }

  addBookToCart(customerId: number, bookId: number): Observable<any> {
    return this.http.put( this.rootUrl + 'customer/' + customerId + '/book/' + bookId , JSON.stringify({}) , this.httpOptions);
  }

  reduceBookFromCart(customerId: number, bookId: number): Observable<any> {
    return this.http.delete( this.rootUrl + 'customer/' + customerId + '/book/reduce/' + bookId , this.httpOptions);
  }

  getCartItems(customerId: number): Observable<any> {
    return this.http.get(this.rootUrl + 'customer/' + customerId + '/cart');
  }

  deleteBookFromCart(customerId: number , bookId: number) {
    return this.http.delete(this.rootUrl + 'customer/' + customerId + '/book/' + bookId , this.httpOptions);
  }

  makeOrder(customerId: number) {
    return this.http.put(this.rootUrl + 'customer/' + customerId + '/order/' , JSON.stringify({}), this.httpOptions);
  }
}
