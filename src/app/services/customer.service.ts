import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginEntity } from '../models/loginEntity';
import { Customer } from '../models/customer';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private rootUrl: String = 'http://localhost:8080/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*'
    })
   };
  constructor(private http: HttpClient) { }

  getCustomerIfExist(entity: LoginEntity): Observable<any> {
    return this.http.post(this.rootUrl + 'login', JSON.stringify(entity), this.httpOptions );
  }
  getExistingCustomerFromId(id: number): Observable<any> {
    return this.http.get(this.rootUrl + '/customer/' + id);
  }
}
