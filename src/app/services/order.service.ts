import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private rootUrl: String = 'http://localhost:8080/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
    })
   };
  constructor(private http: HttpClient) {}

  getCustomerOrders(customerId: number): Observable<any> {
    return this.http.get(this.rootUrl + 'customer/' + customerId + '/order/');
  }
}
