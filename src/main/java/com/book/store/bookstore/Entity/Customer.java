package com.book.store.bookstore.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Customer {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private int id;
    private String name;
    private String email;
    private String saltedPassword;
    private String address;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    @JsonIgnore
    private CustomerCart customerCart;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<CustomerOrder> customerOrders;

    public Customer(){}
    public Customer(int id,String name, String email, String saltedPassword,String address){
        this.id=id;
        this.email=email;
        this.name=name;
        this.saltedPassword=saltedPassword;
        this.address=address;
    }
    public Customer(String name, String email,String saltedPassword,String address){
        this.email=email;
        this.name=name;
        this.saltedPassword=saltedPassword;
        this.address=address;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
