package com.book.store.bookstore.Repository;

import com.book.store.bookstore.Entity.CustomerCart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerCartRepository extends JpaRepository<CustomerCart,Integer> {
}
