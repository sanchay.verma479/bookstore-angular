package com.book.store.bookstore.Repository;

import com.book.store.bookstore.Entity.CustomerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerOrderRepository extends JpaRepository<CustomerOrder,Integer> {

}
