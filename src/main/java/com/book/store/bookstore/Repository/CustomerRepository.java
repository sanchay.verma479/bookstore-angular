package com.book.store.bookstore.Repository;

import com.book.store.bookstore.Entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer,Integer> {
    Optional<Customer> findByEmailAndSaltedPassword(String email,String saltedPassword);
}
