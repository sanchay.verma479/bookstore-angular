package com.book.store.bookstore.Service;
import com.book.store.bookstore.Entity.Customer;
import com.book.store.bookstore.Entity.CustomerCart;
import com.book.store.bookstore.Entity.CustomerOrder;
import com.book.store.bookstore.Repository.CustomerCartRepository;
import com.book.store.bookstore.Repository.CustomerOrderRepository;
import com.book.store.bookstore.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    private static final String LOGIN_MESSAGE="Login First";
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerCartRepository customerCartRepository;
    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    public String addCustomer(Customer customer){
        try {
            Optional<Customer> optionalCustomer = customerRepository.findById(customer.getId());
            if (optionalCustomer.isPresent()) {
                return "Customer with id already exists";
            } else {
                customer.setCustomerCart(new CustomerCart());
                customerRepository.save(customer);
                return "Customer has been added";
            }
        }
        catch (Exception e){
            return e.getMessage();
        }
    }
    public List<Customer> getAllCustomer(){
        return customerRepository.findAll();
    }
    public Optional<Customer> getCustomerById(int id){
        return customerRepository.findById(id);
    }
    public String updateCustomer(Customer customer){
        Optional<Customer> optionalCustomer = customerRepository.findById(customer.getId());
        if(optionalCustomer.isPresent()){
            customerRepository.save(customer);
            return "Customer updated";
        }
        return "Customer does not exists";
    }
    public String deleteCustomer(int id){
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if(optionalCustomer.isPresent())
        {
            customerRepository.deleteById(id);
            return "Customer deleted";
        }
        return "Unable to find customer";
    }

    public String addBookToCart(int customerId,int bookId){
        Optional<Customer> customer=customerRepository.findById(customerId);
        if(customer.isPresent()){
            Customer c = customerRepository.findById(customerId).orElse(null);
            if(c.getCustomerCart().getBookIntegerMap().containsKey(bookId)){
                c.getCustomerCart().getBookIntegerMap().put(bookId,c.getCustomerCart().getBookIntegerMap().get(bookId)+1);
            }
            else
            {
                c.getCustomerCart().getBookIntegerMap().put(bookId,1);

            }
            customerRepository.save(c);
            return "Quantity increased";
        }
        else{
            return LOGIN_MESSAGE;
        }
    }
    public String reduceBookQuantityFromCart(int customerId,int bookId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer!=null){
            if(customer.getCustomerCart().getBookIntegerMap().containsKey(bookId)){
                int val=customer.getCustomerCart().getBookIntegerMap().get(bookId);
                if(val==1){
                    customer.getCustomerCart().getBookIntegerMap().remove(bookId);
                }
                else{
                    customer.getCustomerCart().getBookIntegerMap().replace(bookId,val-1);
                }
                customerRepository.save(customer);
            }
            else{
                return "Not present in cart";
            }
            return "Quantity decreased";
        }
        else{
            return LOGIN_MESSAGE;
        }
    }

    public String deleteBookFromCart(int customerId,int bookId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer!=null){
            if(customer.getCustomerCart().getBookIntegerMap().containsKey(bookId)){
                    customer.getCustomerCart().getBookIntegerMap().remove(bookId);
                    customerRepository.save(customer);
            }
            else{
                return "Not present in cart";
            }
            return "Book removed from cart";
        }
        else{
            return LOGIN_MESSAGE;
        }
    }
    public String makeOrder(int customerId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer!=null){
            CustomerOrder customerOrder = new CustomerOrder();
            Map<Integer, Integer> map= new HashMap<>();
            customer.getCustomerCart().getBookIntegerMap().forEach((x,y)->map.put(x,y));
            customerOrder.setBookIntegerMap(map);
            customerOrder.setStatus("ongoing");
            customer.getCustomerOrders().add(customerOrder);
            customer.getCustomerCart().setBookIntegerMap(new HashMap<>());
            customerRepository.save(customer);
            return "Order Placed";
        }
        return LOGIN_MESSAGE;
    }
    public List getCustomerOrders(int customerId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer!=null)
            return customer.getCustomerOrders();
        else
            return Arrays.asList("Not found");
    }
    public Map getCustomerCartItems(int customerId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer!=null){
            return customer.getCustomerCart().getBookIntegerMap();
        }
        return null;
    }
    public String deleteCustomerOrder(int customerId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer!=null){
          List<CustomerOrder> list=new ArrayList<>();
          list.addAll(customer.getCustomerOrders());
          List<CustomerOrder> newlist = new ArrayList<>();
          newlist=list.stream().filter(x->x.getStatus().equals("delivered")).collect(Collectors.toList());
          customer.setCustomerOrders(newlist);
          list.removeAll(newlist);
          customerRepository.save(customer);
          customerOrderRepository.deleteAll(list);
          return "Order canceled";
        }
        else
            return "Cutomer not found";
    }
    public String deleteCustomerOrderFromOrderId(int customerId, int orderId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer!=null){
            Optional<CustomerOrder> customerOrder=customerOrderRepository.findById(orderId);
            if(customerOrder.isPresent()){
                customerOrderRepository.delete(customerOrder.get());
            }
            else{
                return "No such order found";
            }
        }
        return "Customer not found";
    }
    public Optional<Customer> getCustomerFromEmailAndPassword(String email, String password){
        return customerRepository.findByEmailAndSaltedPassword(email,password);
    }
}
